﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Shapes;

public class ActiveGlow : MonoBehaviour
{
    Sequence pulseSequence;
    Disc disc;

    public float normalRadius = 1.2f;
    public float bigRadius = 1.5f;
    public float pulseDuration = 0.5f;

    // Start is called before the first frame update
    void Start()
    {
        disc = GetComponent<Disc>();
        
        SetUpPulseAnimation();
        pulseSequence.Pause();
        disc.Radius = 0f;
    }

    [ContextMenu("Reset Pulse Animation")]
    void ResetPulseAnimation() {
        pulseSequence.Kill();
        SetUpPulseAnimation();
    }

    public void AnimateIn() {
        DOTween.Sequence()
            .Append(disc.DORadius(normalRadius, 0.2f))
            .AppendCallback(() => pulseSequence.Play());
    }

    public void AnimateOut() {
        pulseSequence.Pause();
        disc.DORadius(0f, 0.2f);
    }

    void SetUpPulseAnimation() {
        pulseSequence = DOTween.Sequence();

        pulseSequence
            .Append(disc.DORadius(normalRadius, pulseDuration))
            .Append(disc.DORadius(bigRadius, pulseDuration))
            .SetLoops(-1);
    }
}
