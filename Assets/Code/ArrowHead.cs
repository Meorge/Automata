﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ArrowHead : BaseUIElement
{

    private ArrowLine arrowLine;
    private StateTransition stateTransition;

    private bool isBeingDragged = false;

    void Start() {
        arrowLine = transform.parent.GetComponent<ArrowLine>();
        stateTransition = transform.parent.GetComponent<StateTransition>();
    }

    void LateUpdate()
    {
        // Calculate the angle between the two points in the line
        float angle = Mathf.Atan2(arrowLine.line.End.y, arrowLine.line.End.x);

        transform.localEulerAngles = new Vector3(0f,0f,angle * Mathf.Rad2Deg);
        transform.position = arrowLine.line.transform.TransformPoint(arrowLine.line.End);
    }

    void OnMouseEnter() {
        Player.Instance.currentElement = this;
        transform.DOScale(1.2f, 0.1f);
    }

    void OnMouseExit() {
        if (Player.Instance.currentElement == this && !isBeingDragged)
            Player.Instance.currentElement = null;
        transform.DOScale(1f, 0.1f);
    }

    void OnMouseDown() {
        isBeingDragged = true;
        Debug.Log("mouse down");
        transform.DOScale(1f, 0.1f);
    }

    void OnMouseDrag() {
        if (stateTransition.destinationNode != null) {
            FMODUnity.RuntimeManager.PlayOneShot("event:/Connection Deconfirm");
            stateTransition.destinationNode?.incomingTransitions.Remove(stateTransition);
            stateTransition.destinationNode = null;
            Player.Instance.currentStateTransition = stateTransition;
            arrowLine.Temporary = true;
        }
    }

    void OnMouseUp() {
        Debug.Log("mouse up");
        isBeingDragged = false;
        if (Player.Instance.hoveringStateNode == null) Player.Instance.CancelAddNewStateTransition();
        else {
            Player.Instance.CommitNewStateTransition(Player.Instance.hoveringStateNode);
        }
        
    }
}
