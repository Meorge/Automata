﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Shapes;

public class ArrowLine : MonoBehaviour
{
    public Line line { get; private set; }

    [SerializeField]
    private Triangle arrowHead;


    private bool temporary = false;

    public bool Temporary {
        get { return temporary; }
        set {
            line.Dashed = value;
            temporary = value;
        }
    }

    void Awake()
    {
        line = GetComponent<Line>();
        Temporary = true;
    }

    [ContextMenu("Toggle temporary")]
    void ToggleTemporary() {
        Temporary = !Temporary;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        // update for temporary line
        if (Temporary) {
            line.DashOffset += Time.deltaTime;
            if (line.DashOffset >= 1) line.DashOffset -= 1;
        }
    }

    public void SetOriginPoint(Vector3 originPoint) {
        // Make sure everything's got a z value of 1
        originPoint.z = 1f;
        Vector3 originalPos = transform.position;
        originalPos.z = 1f;

        // calculate our delta
        Vector3 delta = line.transform.InverseTransformVector(originPoint - originalPos);
        delta.z = 1f;
        
        transform.position = originPoint;
        line.End = new Vector3(line.End.x - delta.x, line.End.y - delta.y, 1f);
    }

    public void SetEndPoint(Vector3 endPoint) {
        // Make sure everything's got a z value of 1
        endPoint.z = 1f;
        line.End = transform.InverseTransformPoint(endPoint);
    }

    public Vector3 GetMidpoint() {
        Vector3 endA = transform.position;
        Vector3 endB = transform.TransformPoint(line.End);

        Vector3 midpoint = (endA + endB) / 2f;
        return midpoint;
    }
}
