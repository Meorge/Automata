﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorsManager : MonoBehaviour
{
    public static ColorsManager Instance { get; private set; }

    [Header("Tests")]
    public Color testPassedColor = Color.green;
    public Color testFailedColor = Color.red;

    void Awake() {
        if (Instance == null) Instance = this;
        else Destroy(gameObject);
    }
}
