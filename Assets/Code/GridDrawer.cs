﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Shapes;

public class GridDrawer : MonoBehaviour
{
    [SerializeField]
    Camera camera;

    public float lineThickness = 1f;
    public Color lineColor = Color.red;
    public float zPos = 10f;
    public float xStart = -10f;
    public float xEnd = 10f;
    public float yStart = -10f;
    public float yEnd = 10f;

    public float lineDensity;


    void Start()
    {
        Camera.onPostRender += OnPostRender;
    }

    void Update() {
        // TODO: make this code not suck
        float spaceBetweenLinesX = (xEnd - xStart) / lineDensity;
        float spaceBetweenLinesY = (yEnd - yStart) / lineDensity;

        Vector3 delta = (Camera.main.transform.position - transform.position);

        if (Mathf.Abs(delta.x) >= Mathf.Abs(spaceBetweenLinesX)) {
            transform.Translate(new Vector3(Mathf.Floor(delta.x / spaceBetweenLinesX) * spaceBetweenLinesX, 0f, 0f), Space.World);
        }

        if (Mathf.Abs(delta.y) >= Mathf.Abs(spaceBetweenLinesY)) {
            transform.Translate(new Vector3(0f, Mathf.Floor(delta.y / spaceBetweenLinesY) * spaceBetweenLinesY, 0f), Space.World);
        }
    }


    void OnPostRender(Camera cam) {
        if (cam != camera) return;

        Draw.Matrix = transform.localToWorldMatrix;
        Draw.BlendMode = ShapesBlendMode.Transparent;
        Draw.LineGeometry = LineGeometry.Flat2D;
        Draw.LineThickness = lineThickness;
        Draw.Color = lineColor;

        for (int i = 0; i <= lineDensity + 2; i++) {
            float x = xStart + i * (xEnd - xStart) / (lineDensity + 2);
            Vector3 startPoint = new Vector3(x, yStart, zPos);
            Vector3 endPoint = new Vector3(x, yEnd, zPos);
            Draw.Line(startPoint, endPoint);
        }

        for (int i = 0; i <= lineDensity + 2; i++) {
            float y = yStart + i * (yEnd - yStart) / (lineDensity + 2);
            Vector3 startPoint = new Vector3(xStart, y, zPos);
            Vector3 endPoint = new Vector3(xEnd, y, zPos);
            Draw.Line(startPoint, endPoint);
        }
    }

    void OnDestroy() {
        Camera.onPostRender -= OnPostRender;
    }
}
