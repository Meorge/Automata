﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LevelManager : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI levelNameLabel, levelDescriptionLabel;

    public LevelItem currentLevel { get; private set; }



    [Header("Testing stuff")]
    public LevelItem testLevel;

    void Start() {
        LoadLevel(testLevel);
    }
    public void LoadLevel(LevelItem level) {
        this.currentLevel = level;
        Player.Instance.LoadTestSuite(this.currentLevel.tests);
        UpdateLevelInfoUI();
    }

    void UpdateLevelInfoUI() {
        if (this.levelNameLabel != null) {
            this.levelNameLabel.text = this.currentLevel.name;
        } else {
            Debug.LogError("Level name label is null!");
        }

        if (this.levelDescriptionLabel != null) {
            this.levelDescriptionLabel.text = this.currentLevel.description;
        } else {
            Debug.LogError("Level description label is null!");
        }


        Player.Instance.alphabet = this.currentLevel.alphabet;
    }
}
