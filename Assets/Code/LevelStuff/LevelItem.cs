﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName="New Level", menuName="Automata/Level", order=0)]
public class LevelItem : ScriptableObject
{
    public string levelName = "Level";
    [TextArea]
    public string description = "Level description goes here";
    public string alphabet = "ab";
    public List<Test> tests = new List<Test>();
}

[System.Serializable]
public class Test {
    public string testString = "";
    public bool shouldPass = false;

    [HideInInspector]
    public TestGUI guiItem;
}