﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;

public class Player : MonoBehaviour
{
    public static Player Instance { get; private set; }

    public Vector2 mousePos { get; private set; }
    public bool isDraggingLMB { get; private set; }
    private Vector3 offset;

    [Header("State Nodes")]
    [SerializeField]
    private StateNode stateNodePrefab;

    [SerializeField]
    private Transform stateNodeContainer;

    public StateNode startNode;

    [SerializeField]
    public List<StateNode> stateNodes = new List<StateNode>();
    public List<StateTransition> stateTransitions = new List<StateTransition>();


    [Header("State Transitions")]
    [SerializeField]
    private StateTransition stateTransitionPrefab;
    [SerializeField]
    private Transform stateTransitionContainer;
    public StateTransition currentStateTransition;


    [Header("Readers")]
    public List<Reader> readers = new List<Reader>();

    public StateNode hoveringStateNode = null;

    [Header("Buttons")]
    [SerializeField]
    private ShapesPlayStopButton playButton;
    [SerializeField]
    private ShapesButton stopButton;

    [SerializeField]
    private ShapesDragOffButton addNodeButton;

    [SerializeField]
    public BaseUIElement currentElement = null;
    int nodeIndex = 0;

    [Header("Test Stuff")]
    [SerializeField]
    private Transform testGUIContainer;

    [SerializeField]
    private TestGUI testGUIPrefab;

    // Language stuff (probably temporary)
    public string alphabet = "ab";
    public string testString = "abba";
    public List<Test> tests = new List<Test>();
    
    [SerializeField]
    private int currentTestIndex = -1;



    private bool _isRunning = false;

    public bool IsRunning {
        get {
            return _isRunning;
        }

        set {
            _isRunning = value;
        }
    }
    
    void Awake() {
        if (Instance != null) {
            Debug.LogError("you're trying to create a player when a player already (supposedly) exists! this is bad and you should feel bad about yourself");
            Destroy(gameObject);
            return;
        }

        Instance = this;
    }

    void Start() {
        playButton.clicked += StartTestSuite;
        addNodeButton.clicked += AddNodeDragging;
    }

    void Update() {
        if (Input.GetKeyDown(KeyCode.RightArrow)) {
            NextStepLogic();
        }

        if (Input.GetKeyDown(KeyCode.Return)) {
            print(StateMachineSerializer.SerializeStateMachine());
        }

        if (currentStateTransition != null) {
            Vector3 endPos = Camera.main.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, stateTransitionContainer.position.z));

            Vector3 srcNodePos = currentStateTransition.sourceNode.transform.position;
            srcNodePos.z = stateTransitionContainer.position.z;
            currentStateTransition.arrowLine.SetOriginPoint(EndPoint(endPos, srcNodePos, 1f));
            if (hoveringStateNode == null) {
                currentStateTransition.arrowLine.SetEndPoint(endPos);
            } else {
                currentStateTransition.arrowLine.SetEndPoint(EndPoint(currentStateTransition.sourceNode.transform.position, hoveringStateNode.transform.position, 1.5f));
            }
        }
    }

    public static Vector3 EndPoint(Vector3 nodeA, Vector3 nodeB, float offset) {
        // Let's figure out the endpoint of the arrow...
        Vector3 deltaVector = -(nodeB - nodeA).normalized;
        return nodeB + deltaVector * offset;
    }
    public void OnMouseMove(InputAction.CallbackContext context) {
        Vector2 vec = context.ReadValue<Vector2>();

        Vector2 prevMousePos = mousePos;
        mousePos = vec;

        Vector2 delta = mousePos - prevMousePos;

        Vector3 deltaVec3 = new Vector3(delta.x, delta.y, 0f);

        if (hoveringStateNode == null && currentStateTransition == null && currentElement == null && isDraggingLMB) {
            Camera.main.transform.position = Vector3.Lerp(Camera.main.transform.position, Camera.main.transform.position + deltaVec3 * -0.9f, Time.deltaTime * 2f);
        } else {
        }
    }

    public void OnLeftMouseButton(InputAction.CallbackContext context) {
        if (context.phase == InputActionPhase.Performed) {
            hoveringStateNode?.OnMouseDownEvent();

            offset = Camera.main.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, Camera.main.transform.position.z));
            offset.z = Camera.main.transform.position.z;
            isDraggingLMB = true;
        }
        if (context.phase == InputActionPhase.Canceled) {
            hoveringStateNode?.OnMouseUpEvent();
            isDraggingLMB = false;
        }
    }

    // public void OnRightMouseClick(InputAction.CallbackContext context) {
    //     if (context.phase == InputActionPhase.Performed) {
    //         AddNewNode(atPosition: mousePos);
    //     }
    // }

    void AddNodeDragging() {
        hoveringStateNode = AddNewNode(mousePos);
        hoveringStateNode.OnMouseDownEvent();
    }

    StateNode AddNewNode(Vector2 atPosition) {
        if (hoveringStateNode != null) {
            Debug.LogWarning("Attempting to place a state node on top of another state node... aborting");
            return null;
        }
        StateNode newStateNode = Instantiate(stateNodePrefab);
        newStateNode.transform.SetParent(stateNodeContainer, false);
        newStateNode.gameObject.name = $"{nodeIndex}";
        newStateNode.label.text = $"{nodeIndex}";
        newStateNode.id = nodeIndex;
        nodeIndex++;
        Vector3 newNodePos = Camera.main.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, 0f));
        newNodePos.z = stateNodeContainer.transform.position.z;
        newStateNode.transform.position = newNodePos;

        return newStateNode;
    }


    public void AddNewStateTransition(StateNode fromNode) {
        currentStateTransition = Instantiate(stateTransitionPrefab);
        currentStateTransition.transform.SetParent(stateTransitionContainer, false);

        Vector3 p = fromNode.transform.position;
        p.z = stateTransitionContainer.position.z;
        currentStateTransition.arrowLine.SetOriginPoint(EndPoint(Camera.main.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, stateTransitionContainer.position.z)), p, 1f));

        currentStateTransition.sourceNode = fromNode;

        FMODUnity.RuntimeManager.PlayOneShot("event:/Connection Start");
    }

    public void CancelAddNewStateTransition() {
        print("in cancel new state transition");
        currentStateTransition?.DestroyTransition();
        currentStateTransition = null;
    }

    public void CommitNewStateTransition(StateNode toNode) {
        if (currentStateTransition == null) return;

        StateNode fromNode = currentStateTransition.sourceNode;

        currentStateTransition.destinationNode = toNode;

        // Let's do some checks to make sure this is a valid transition
        // First, we make sure this transition's source node is not the same as its destination node
        if (fromNode == toNode) {
            Debug.LogWarning("Can't have it point to itself!");
            CancelAddNewStateTransition();
            return;
        }

        // Second, we make sure this transition's source node doesn't already have a transition pointing
        // to the same destination node
        if (currentStateTransition.sourceNode.outgoingTransitions.Any(
            transition => 
                (transition.destinationNode == toNode && transition != currentStateTransition)
                )
            )
        {
            Debug.LogWarning("This transition already exists!");
            CancelAddNewStateTransition();
            return;
        }
        
        if (!currentStateTransition.sourceNode.outgoingTransitions.Contains(currentStateTransition))
            currentStateTransition.sourceNode.outgoingTransitions.Add(currentStateTransition);

        if (!hoveringStateNode.incomingTransitions.Contains(currentStateTransition))
            hoveringStateNode.incomingTransitions.Add(currentStateTransition);

        currentStateTransition.CommitTransition();

        FMODUnity.RuntimeManager.PlayOneShot("event:/Connection Confirm");

        currentStateTransition = null;


        // Let's see if these two nodes have a two-way connection
        // Two-way connection exists if for any connection in the source node, there's also a connection
        // that goes the opposite way
        bool twoWayConnection = false;
        foreach (StateTransition t1 in fromNode.outgoingTransitions) {
            if (toNode.outgoingTransitions.Any(t2 => t2.sourceNode == t1.destinationNode && t2.destinationNode == t1.sourceNode)) {
                twoWayConnection = true;
                break;
            }
        }

        // TODO: handle drawing stuff for a two-way connection

        Debug.Log($"Two way connection between these nodes: {twoWayConnection}");
    }


    public void LoadTestSuite(List<Test> tests) {
        this.tests = new List<Test>(tests);

        foreach (Transform oldTest in testGUIContainer) {
            Destroy(oldTest);
        }

        foreach (Test test in this.tests) {
            TestGUI newTestGUI = Instantiate(testGUIPrefab);
            newTestGUI.transform.SetParent(testGUIContainer, false);
            newTestGUI.gameObject.name = test.testString + (test.shouldPass ? " (should accept)" : " (should reject)");
            newTestGUI.SetTest(test);
        }
    }



    // LOGIC TIME
    public void StartLogic(string testString) {
        // if (IsRunning) {
        //     Debug.Log("Stop logic");
        //     StopLogic();
        //     return;
        // }
        IsRunning = true;
        Reader freshReader = new Reader(testString);
        readers.Add(freshReader);
        startNode.AddReader(freshReader);
    }

    public void StopLogic() {
        ClearAllReaders();
        IsRunning = false;
    }

    public void ClearAllReaders() {
        foreach (StateNode node in stateNodes) {
            node.RemoveAllReaders();
        }

        List<Reader> tempReaders = new List<Reader>(readers);
        foreach (Reader r in tempReaders) {
            readers.Remove(r);
        }
    }

    public void NextStepLogic() {
        Debug.Log("Next step");
        foreach (StateNode node in stateNodes) {
            node.EvaluateNextStep();
        }

        // Undirty the readers
        foreach (Reader r in readers) {
            r.isDirty = false;
        }

        List<Reader> tempReaders = new List<Reader>(readers);
        foreach (Reader r in tempReaders) {
            if (r.currentNode == null) readers.Remove(r);
        }

        // Check if no more readers
        if (readers.Count == 0) {
            // not running anymore
            Debug.LogWarning("No more readers - string rejected?");
            TestCompleted(false);
            // if (tests[currentTestIndex].shouldPass == false) {
            //     Debug.Log("This string should have been rejected, so the test passed");
            //     TestCompleted();
            // } else {
            //     // Should have been accepted, but it wasn't
            //     // so stop
            //     TestCompleted();
            // }
        }
    }

    public void StartTestSuite() {
        currentTestIndex = -1;
        RunNextTest();
    }

    public void TestCompleted(bool accepted = true) {
        ClearAllReaders();
        
        if (currentTestIndex < tests.Count) {
            tests[currentTestIndex].guiItem.TestCompleted(accepted);
            Debug.Log($"Test #{currentTestIndex} with test string {tests[currentTestIndex].testString} completed!");
        }
        if (!RunNextTest()) {
            IsRunning = false;
            playButton.Toggled = false;
        }
    }

    public bool RunNextTest() {
        currentTestIndex++;
        
        if (currentTestIndex >= tests.Count) {
            Debug.Log("ALL TESTS PASSED!");
            return false;
        }

        Debug.Log($"Running test #{currentTestIndex}, with test string {tests[currentTestIndex].testString}");
        StartLogic(tests[currentTestIndex].testString);
        return true;

    }
}
