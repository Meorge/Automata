using System;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Reader {
    public string readingString = "";

    public StateNode currentNode = null;

    public bool isDirty = false;

    public Reader(string readingString) {
        this.readingString = readingString;
    }

    public char PeekNextCharacterFromString() {
        return readingString[0];
    }

    public char PopNextCharacterFromString() {
        char nextChar = readingString[0];
        readingString = readingString.Substring(1, readingString.Length - 1);
        return nextChar;
    }

    public bool IsComplete() {
        return readingString.Length == 0;
    }

    public Reader MakeCopy() {
        Reader newReader = new Reader(readingString);
        newReader.currentNode = currentNode;
        newReader.isDirty = isDirty;
        return newReader;
    }
}