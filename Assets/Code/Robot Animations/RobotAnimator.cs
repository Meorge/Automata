﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobotAnimator : MonoBehaviour
{
    public List<RobotEyeAnimator> eyeAnimators = new List<RobotEyeAnimator>();

    void Update() {
        if (Input.GetKeyDown(KeyCode.Q)) SetEmotionNormal();
        else if (Input.GetKeyDown(KeyCode.W)) SetEmotionSuspicious();
        else if (Input.GetKeyDown(KeyCode.E)) SetEmotionSurprised();
        else if (Input.GetKeyDown(KeyCode.R)) SetEmotionPleased();
        else if (Input.GetKeyDown(KeyCode.T)) SetEmotionAngry();
        else if (Input.GetKeyDown(KeyCode.Y)) SetEmotionSad();
    }

    [ContextMenu("Set emotion normal")]
    public void SetEmotionNormal() {
        foreach (RobotEyeAnimator a in eyeAnimators) a.SetEmotionNormal();
    }

    [ContextMenu("Set emotion suspicious")]
    public void SetEmotionSuspicious() {
        foreach (RobotEyeAnimator a in eyeAnimators) a.SetEmotionSuspicious();
    }

    [ContextMenu("Set emotion surprised")]
    public void SetEmotionSurprised() {
        foreach (RobotEyeAnimator a in eyeAnimators) a.SetEmotionSurprised();
    }

    [ContextMenu("Set emotion pleased")]
    public void SetEmotionPleased() {
        foreach (RobotEyeAnimator a in eyeAnimators) a.SetEmotionPleased();
    }

    [ContextMenu("Set emotion angry")]
    public void SetEmotionAngry() {
        foreach (RobotEyeAnimator a in eyeAnimators) a.SetEmotionAngry();
    }

    [ContextMenu("Set emotion sad")]
    public void SetEmotionSad() {
        foreach (RobotEyeAnimator a in eyeAnimators) a.SetEmotionSad();
    }
}
