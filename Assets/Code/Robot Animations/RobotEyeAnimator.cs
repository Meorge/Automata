﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Shapes;
using DG.Tweening;

public class RobotEyeAnimator : MonoBehaviour
{
    Sequence normalSequence, squintSequence, surprisedSequence;

    Sequence currentSequence;

    [SerializeField]
    private RegularPolygon bottomEyebrow, topEyebrow;

    [Header("Squint animation")]
    public float squintEyeScale = 0.4f;

    [Header("Surprised animation")]
    public float surprisedEyeScale = 1.2f;


    // Start is called before the first frame update
    void Start()
    {
        SetUpNormalSequence();
        SetUpSquintSequence();
        SetUpSurprisedSequence();

        SetEmotionNormal();
    }

    void SetUpNormalSequence() {
        normalSequence = DOTween.Sequence()
            .Append(transform.DOScaleY(0.1f, 0.1f))
            .Append(transform.DOScaleY(1f, 0.1f))
            .AppendInterval(0.5f)
            .Append(transform.DOScaleY(0.1f, 0.1f))
            .Append(transform.DOScaleY(1f, 0.1f))
            .AppendInterval(3f)
            .Append(transform.DOScaleY(0.1f, 0.1f))
            .Append(transform.DOScaleY(1f, 0.1f))
            .AppendInterval(4f)
            .SetLoops(-1)
            .Pause();
    }

    void SetUpSquintSequence() {
        squintSequence = DOTween.Sequence()
            .Append(transform.DOScaleY(0.1f, 0.1f))
            .Append(transform.DOScaleY(squintEyeScale, 0.1f))
            .AppendInterval(0.5f)
            .Append(transform.DOScaleY(0.1f, 0.1f))
            .Append(transform.DOScaleY(squintEyeScale, 0.1f))
            .AppendInterval(3f)
            .Append(transform.DOScaleY(0.1f, 0.1f))
            .Append(transform.DOScaleY(squintEyeScale, 0.1f))
            .AppendInterval(4f)
            .SetLoops(-1)
            .Pause();
    }

    void SetUpSurprisedSequence() {
        surprisedSequence = DOTween.Sequence()
            .Append(transform.DOScaleY(0.1f, 0.1f))
            .Append(transform.DOScaleY(surprisedEyeScale, 0.1f))
            .AppendInterval(0.5f)
            .Append(transform.DOScaleY(0.1f, 0.1f))
            .Append(transform.DOScaleY(surprisedEyeScale, 0.1f))
            .AppendInterval(3f)
            .Append(transform.DOScaleY(0.1f, 0.1f))
            .Append(transform.DOScaleY(surprisedEyeScale, 0.1f))
            .AppendInterval(4f)
            .SetLoops(-1)
            .Pause();
    }

    [ContextMenu("Set emotion normal")]
    public void SetEmotionNormal() {
        this.currentSequence.Pause();
        topEyebrow.transform.DOScaleY(0f, 0.2f);
        bottomEyebrow.transform.DOScaleY(0f, 0.2f);
        transform.DOScaleY(1f, 0.1f);
        this.currentSequence = normalSequence.Play();
    }

    [ContextMenu("Set emotion suspicious")]
    public void SetEmotionSuspicious() {
        this.currentSequence.Pause();
        topEyebrow.transform.DOScaleY(0.1f, 0.2f);
        bottomEyebrow.transform.DOScaleY(0.5f, 0.2f);
        transform.DOScaleY(squintEyeScale, 0.1f);
        this.currentSequence = squintSequence.Play();
    }

    [ContextMenu("Set emotion surprised")]
    public void SetEmotionSurprised() {
        this.currentSequence.Pause();
        topEyebrow.transform.DOScaleY(0f, 0.2f);
        bottomEyebrow.transform.DOScaleY(0f, 0.2f);
        transform.DOScaleY(surprisedEyeScale, 0.1f);
        this.currentSequence = surprisedSequence.Play();
    }

    [ContextMenu("Set emotion pleased")]
    public void SetEmotionPleased() {
        this.currentSequence.Pause();
        topEyebrow.transform.DOScaleY(0f, 0.2f);
        bottomEyebrow.transform.DOScaleY(1f, 0.2f);
        transform.DOScaleY(squintEyeScale, 0.1f);
        this.currentSequence = squintSequence.Play();
    }

    [ContextMenu("Set emotion pleased")]
    public void SetEmotionAngry() {
        this.currentSequence.Pause();
        topEyebrow.transform.DOScaleY(0.7f, 0.2f);
        bottomEyebrow.transform.DOScaleY(0.45f, 0.2f);
        transform.DOScaleY(1f, 0.1f);
        this.currentSequence = normalSequence.Play();
    }

    [ContextMenu("Set emotion pleased")]
    public void SetEmotionSad() {
        this.currentSequence.Pause();
        topEyebrow.transform.DOScaleY(1.7f, 0.2f);
        bottomEyebrow.transform.DOScaleY(0f, 0.2f);
        transform.DOScaleY(1f, 0.1f);
        this.currentSequence = normalSequence.Play();
    }
}
