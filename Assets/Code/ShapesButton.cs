﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Shapes;
using DG.Tweening;

public class ShapesButton : BaseUIElement
{
    protected Disc background;
    public float depressedScale = 0.9f;
    public float animationDuration = 0.1f;

    private bool visible = false;
    private bool _hidden;
    public bool Hidden {
        get {
            return _hidden;
        }

        set {
            _hidden = value;
            if (_hidden && visible) {
                AnimateOut();
            } else if (!_hidden && visible) {
                AnimateIn();
            }
        }
    }

    protected bool mouseIsOver = false;
    protected bool isDepressed = false;

    // event handling
    public delegate void ButtonEvent();
    public event ButtonEvent clicked;

    virtual public void OnMouseEnter() { if (Player.Instance.isDraggingLMB) return; mouseIsOver = true; Player.Instance.currentElement = this; }
    virtual public void OnMouseExit() { mouseIsOver = false; if (Player.Instance.currentElement == this) Player.Instance.currentElement = null; }

    void Awake() {
        background = GetComponent<Disc>();
    }
    
    void OnMouseDown() {
        isDepressed = true;
        AnimatePushDown();
    }

    virtual public void OnMouseUp() {
        isDepressed = false;
        
        AnimatePopUp();

        if (mouseIsOver) {
            FMODUnity.RuntimeManager.PlayOneShot("event:/Click Button");
            RunFunctions();
        }
    }

    protected void RunFunctions() {
        clicked?.Invoke();
    }

    // Animations
    protected void AnimatePushDown() {
        transform.DOScale(depressedScale, animationDuration);
    }

    protected void AnimatePopUp() {
        transform.DOScale(1f, animationDuration);
        visible = true;
        
    }

    public void AnimateIn() {
        if (_hidden) {SetOut(); return;}
        AnimatePopUp();
    }

    public void AnimateOut() {
        // if (!_hidden) return;
        transform.DOScale(0f, animationDuration);
        visible = false;
    }

    public void SetIn() {
        transform.localScale = Vector3.one;
        visible = true;
    }

    public void SetOut() {
        transform.localScale = Vector3.zero;
        visible = false;
    }
}
