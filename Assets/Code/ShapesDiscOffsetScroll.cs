﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Shapes;

public class ShapesDiscOffsetScroll : MonoBehaviour
{
    private Disc disc;

    public float speed = 1f;

    void Start() { disc = GetComponent<Disc>(); }

    void Update() {
        disc.DashOffset += Time.deltaTime * speed;
        if (disc.DashOffset >= 1) disc.DashOffset -= 1;
    }
}
