﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShapesDragOffButton : ShapesButton
{
    public event ButtonEvent released;
    override public void OnMouseExit() {
        
        base.OnMouseExit();

        if (isDepressed) {
            RunFunctions();
            AnimateOut();
        }
    }

    override public void OnMouseUp() {
        isDepressed = false;

        released?.Invoke();

        AnimateIn();
    }

    void OnMouseDrag() {
    }
}
