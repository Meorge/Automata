﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Shapes;

public class ShapesPlayStopButton : ShapesToggleButton
{
    [SerializeField]
    private RegularPolygon symbol;

    public Color activeSymbolColor, inactiveSymbolColor;


    public override void AnimateCurrentState() {
        Sequence seq = DOTween.Sequence()
            .Append(symbol.transform.DORotate(new Vector3(0,0, Toggled ? -45f : 0f), 0.25f).SetEase(Ease.InOutBack, 2.5f))
            .Insert(0.1f, background.DOColor(Toggled ? activeColor : inactiveColor, 0.2f))
            .Insert(0.1f, symbol.DOColor(Toggled ? activeSymbolColor : inactiveSymbolColor, 0.2f))
            .InsertCallback(0.1f, () => symbol.Sides = Toggled ? 4 : 3);
    }

}
