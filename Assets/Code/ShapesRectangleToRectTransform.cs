﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Shapes;

[ExecuteAlways]
[RequireComponent(typeof(Rectangle))]
public class ShapesRectangleToRectTransform : MonoBehaviour
{
    private Rectangle rectangle;
    private RectTransform rect;

    void Awake() {
        rectangle = GetComponent<Rectangle>();
        rect = GetComponent<RectTransform>();
    }

    // Update is called once per frame
    void Update()
    {
        rectangle.Width = rect.rect.width;
        rectangle.Height = rect.rect.height;
        rectangle.Pivot = RectPivot.Corner;
    }
}
