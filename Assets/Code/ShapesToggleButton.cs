﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShapesToggleButton : ShapesButton
{
    private bool _toggled = false;
    public bool Toggled {
        get {
            return _toggled;
        }

        set {
            _toggled = value;
            toggled?.Invoke(_toggled);
            AnimateCurrentState();
        }
        
    }

    public delegate void ToggleButtonEvent(bool toggled);
    public event ToggleButtonEvent toggled;

    public Color activeColor = Color.white;
    public Color inactiveColor = Color.gray;

    void Start() {
        clicked += ToggleState;
        Toggled = false;
    }

    void ToggleState() {
        Toggled = !Toggled;
    }

    public virtual void AnimateCurrentState() {
        Debug.Log($"toggled is {Toggled}");

        background.DOColor(_toggled ? activeColor : inactiveColor, 0.1f);

    }
}
