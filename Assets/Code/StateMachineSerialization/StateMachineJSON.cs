﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

[System.Serializable]
public class StateMachineSerializable
{
    public List<StateMachineNodeSerializable> nodes;
    public List<StateTransitionSerializable> transitions;
}

[System.Serializable]
public class StateMachineNodeSerializable
{
    public Vector2 position;
    public bool isAcceptState;
    public int id;
}

[System.Serializable]
public class StateTransitionSerializable
{
    public int originNodeIndex;
    public int destinationNodeIndex;
    public string allowedCharacters;
}
