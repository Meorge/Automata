using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public class StateMachineSerializer {
    public static string SerializeStateMachine() {
        // First we need to create the structure...

        var stateMachine = new StateMachineSerializable();

        stateMachine.nodes = new List<StateMachineNodeSerializable>();
        foreach (var node in Player.Instance.stateNodes) {
            stateMachine.nodes.Add(node.ToSerializable());
        }

        stateMachine.transitions = new List<StateTransitionSerializable>();
        foreach (var trans in Player.Instance.stateTransitions) {
            stateMachine.transitions.Add(trans.ToSerializable());
        }

        return JsonConvert.SerializeObject(stateMachine);
    }
}