﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using TMPro;
using Shapes;
using DG.Tweening;

public class StateNode : BaseUIElement
{
    [SerializeField]
    private Disc mainDisc, innerDisc, shadowDisc;

    [SerializeField]
    private ActiveGlow activeGlow;

    [SerializeField]
    private GameObject startArrow;
    
    [SerializeField]
    private float defaultShadowRadius = 1.2f;
    [SerializeField]
    private float raisedShadowRadius = 1.5f;
    [SerializeField]
    private float raisedShadowOffset = -0.1f;
    [SerializeField]
    private float rotate3DAmount = 10f;

    [SerializeField]
    private float acceptStateRingThickness = 0.01f;

    private bool isBeingDragged = false;

    float multiplier = 0.05f;

    [SerializeField]
    Vector3 screenPos, offset;

    // label stuff
    public TextMeshPro label;

    // Button stuff
    [Header("Button stuff")]
    public float detectRadius = 1f;
    [SerializeField]
    private ShapesButton deleteNodeButton, setAsAcceptButton;
    
    [SerializeField]
    private ShapesDragOffButton addConnectionButton;

    public List<StateTransition> outgoingTransitions = new List<StateTransition>();
    public List<StateTransition> incomingTransitions = new List<StateTransition>();

    public List<Reader> readers = new List<Reader>();

    [SerializeField]
    float mouseDistance = 0f;

    public bool deletingAllowed = true;

    public bool showStartArrow = false;

    private bool _acceptState = false;

    [SerializeField]
    public bool IsAcceptState {
        get {
            return _acceptState;
        }

        set {
            _acceptState = value;
            AnimateAcceptState();
        }
    }

    public int id = 0;

    public StateMachineNodeSerializable ToSerializable() {
        var node = new StateMachineNodeSerializable();
        node.position = new Vector2(transform.position.x, transform.position.y);
        node.isAcceptState = IsAcceptState;
        node.id = id;
        return node;
    }

    // Start is called before the first frame update
    void Start()
    {
        // isBeingDragged = false;

        shadowDisc.Radius = defaultShadowRadius;
        innerDisc.Thickness = 0;


        deleteNodeButton.clicked += DeleteNode;
        setAsAcceptButton.clicked += ToggleAcceptState;
        addConnectionButton.clicked += AddConnection;
        addConnectionButton.released += CommitConnection;

        Player.Instance.stateNodes.Add(this);

        AnimateAcceptState();
        AnimateIn();
    }

    void OnDestroy() {
        Player.Instance.stateNodes.Remove(this);
    }


    void Update() {
        if (isBeingDragged) {
            Vector3 newPosition = Camera.main.ScreenToWorldPoint(Player.Instance.mousePos) + offset;
            transform.position = newPosition;

            foreach (StateTransition line in outgoingTransitions) {
                if (line.destinationNode != null) line.arrowLine.SetOriginPoint(Player.EndPoint(transform.position, line.destinationNode.transform.position, 1f));
            }
        }

        // We only want to maybe show the buttons if the user isn't currently dragging a new connection out
        if (Player.Instance.currentStateTransition == null) {
            float prevMouseDistance = mouseDistance;

            mouseDistance = ((Vector2)transform.position - (Vector2)Camera.main.ScreenToWorldPoint(new Vector3(Player.Instance.mousePos.x, Player.Instance.mousePos.y, transform.position.z))).magnitude;

            if (mouseDistance < detectRadius && prevMouseDistance >= detectRadius && !Player.Instance.IsRunning) {
                // animate buttons in
                deleteNodeButton.AnimateIn();
                addConnectionButton.AnimateIn();
                setAsAcceptButton.AnimateIn();
            } else if ((mouseDistance > detectRadius && prevMouseDistance <= detectRadius) || Player.Instance.IsRunning) {
                // animate buttons out
                deleteNodeButton.AnimateOut();
                addConnectionButton.AnimateOut();
                setAsAcceptButton.AnimateOut();
            }
        }

        // Hide the delete button if this node can't be deleted
        deleteNodeButton.Hidden = !deletingAllowed;

        // Show start arrow maybe?
        startArrow.SetActive(showStartArrow);


    }

    void OnMouseEnter() {
        if (!Player.Instance.isDraggingLMB) {
            Player.Instance.currentElement = this;
        }
        Player.Instance.hoveringStateNode = this;


    }

    void OnMouseExit() {
        if (isBeingDragged) {
            Debug.Log("Mouse exited but we're dragging so don't do anything"); return;
        }

        if (Player.Instance.hoveringStateNode == this) {

            Player.Instance.hoveringStateNode = null;
        }

        if (Player.Instance.currentElement == this) {
            Debug.Log("mouse exited");
            Player.Instance.currentElement = null;
        }
    }


    public void OnMouseDownEvent() {
        isBeingDragged = true;
        screenPos = Camera.main.WorldToScreenPoint(transform.position);
        offset = transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Player.Instance.mousePos.x, Player.Instance.mousePos.y, transform.position.z));


        // Animate the shadow on the disc
        AnimateLiftUp();
    }

    public void OnMouseUpEvent() {
        isBeingDragged = false;

        

        // Animate the shadow on the disc
        AnimateSetDown();
    }


    void AddConnection() {
        Player.Instance.AddNewStateTransition(this);
    }

    void CommitConnection() {
        // TODO: make it not just delete if it's ok
        print($"commit connection, hoveringStateNode=<{Player.Instance.hoveringStateNode}>");
        if (Player.Instance.hoveringStateNode == null)
            Player.Instance.CancelAddNewStateTransition();
        else
            Player.Instance.CommitNewStateTransition(Player.Instance.hoveringStateNode);
    }

    void DeleteNode() {
        print("delete node!");

        List<StateTransition> incoming = new List<StateTransition>(incomingTransitions);
        List<StateTransition> outgoing = new List<StateTransition>(outgoingTransitions);
        incoming.ForEach((x) => x.DestroyTransition());
        outgoing.ForEach((x) => x.DestroyTransition());
        AnimateOut();
    }

    void ToggleAcceptState() {
        IsAcceptState = !IsAcceptState;
    }


    // Animations
    void AnimateIn() {
        // buttons
        // deleteNodeButton.SetOut();
        // addConnectionButton.SetOut();

        transform.localScale = Vector3.zero;
        transform.DOScale(1f, 0.1f).SetEase(Ease.OutBack);
        FMODUnity.RuntimeManager.PlayOneShot("event:/Node In");
    }

    void AnimateOut() {
        FMODUnity.RuntimeManager.PlayOneShot("event:/Node Out");
        DOTween.Sequence()
            .Append(transform.DOScale(0f, 0.2f).SetEase(Ease.InBack))
            .AppendCallback(() => Destroy(gameObject));
    }

    void AnimateLiftUp() {
        shadowDisc.DORadius(raisedShadowRadius, 0.2f);
        shadowDisc.transform.DOLocalMoveY(raisedShadowOffset, 0.2f);
        transform.DOScale(1.05f, 0.2f);
        transform.DORotate(new Vector3(rotate3DAmount, 0f, 0f), 0.2f);

        FMODUnity.RuntimeManager.PlayOneShot("event:/Node Pick Up");
    }

    void AnimateSetDown() {
        shadowDisc.DORadius(defaultShadowRadius, 0.2f);
        shadowDisc.transform.DOLocalMoveY(0f, 0.2f);
        transform.DOScale(1f, 0.2f);
        transform.DORotate(Vector3.zero, 0.2f);
        FMODUnity.RuntimeManager.PlayOneShot("event:/Node Put Down");
    }


    void AnimateAcceptState() {
        innerDisc.DOThickness(IsAcceptState ? acceptStateRingThickness : 0f, 0.2f).SetEase(IsAcceptState ? Ease.OutBack : Ease.InBack);
    }

    public void AddReader(Reader newReader) {
        readers.Add(newReader);
        Player.Instance.readers.Add(newReader);
        newReader.currentNode = this;
        activeGlow.AnimateIn();
    }

    public void RemoveReaderFromSelf(Reader reader) {
        readers.Remove(reader);
        reader.currentNode = null;
        if (readers.Count <= 0) activeGlow.AnimateOut();
    }

    public void RemoveAllReaders() {
        List<Reader> tempReaders = new List<Reader>(readers);
        foreach (Reader r in tempReaders) {
            RemoveReaderFromSelf(r);
        }
    }
    
    public void EvaluateNextStep() {
        List<Reader> tempReaders = new List<Reader>(readers);
        foreach (Reader r in tempReaders) {
            // Don't bother with dirty transitions (each one only gets to make one move per turn!)
            if (r.isDirty) continue;

            if (r.IsComplete()) {
                RemoveReaderFromSelf(r);

                // Check if this is a win node
                // if so, the string was accepted
                if (IsAcceptState) {
                    Debug.LogWarning("STRING ACCEPTED!");
                    Player.Instance.TestCompleted();
                }

                continue;
            }
            char nextChar = r.PeekNextCharacterFromString();
            // First, check for epsilon transitions!
            List<StateTransition> tempTransitions = new List<StateTransition>(outgoingTransitions);

            // There are no transitions
            if (tempTransitions.Count <= 0) {
                // Delete this reader
                Player.Instance.readers.Remove(r);
                RemoveReaderFromSelf(r);
                continue;
            }
            foreach (StateTransition transition in tempTransitions) {

                Reader copyOfReader = r.MakeCopy();
                
                // Are there epsilon transitions?
                if (transition.AcceptedCharacters == "") {
                    // Epsilon transition time!!
                    copyOfReader.isDirty = true;
                    RemoveReaderFromSelf(r);
                    transition.destinationNode.AddReader(copyOfReader);
                    
                    continue;
                }
                
                // No epsilon transitions
                else if (transition.AcceptedCharacters.Contains(nextChar)) {
                    copyOfReader.PopNextCharacterFromString();
                    // Transition to the new one!
                    copyOfReader.isDirty = true;
                    RemoveReaderFromSelf(r);
                    transition.destinationNode.AddReader(copyOfReader);
                    
                    continue;
                }

                // No transitions at all, so delete this reader :(
                else {
                    Player.Instance.readers.Remove(copyOfReader);
                    RemoveReaderFromSelf(r);
                    continue;
                }

                
            }

        }
    }
}
