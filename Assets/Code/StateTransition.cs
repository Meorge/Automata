﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class StateTransition : MonoBehaviour
{
    [SerializeField]
    public ArrowLine arrowLine { get; private set; }

    public StateNode sourceNode;
    public StateNode destinationNode;

    [SerializeField]
    private TextMeshPro transitionLabel;

    [SerializeField]
    private StateTransitionPicker picker;


    public string AcceptedCharacters {
        get {
            return picker.acceptedCharacters;
        }
    }

    public StateTransitionSerializable ToSerializable() {
        var trans = new StateTransitionSerializable();
        trans.originNodeIndex = sourceNode.id;
        trans.destinationNodeIndex = destinationNode.id;
        trans.allowedCharacters = picker.acceptedCharacters;
        return trans;
    }

    void Awake()
    {
        arrowLine = GetComponent<ArrowLine>();
        Player.Instance.stateTransitions.Add(this);
    }

    void Update() {
        if (sourceNode != null && destinationNode != null) {
            arrowLine.SetOriginPoint(Player.EndPoint(destinationNode.transform.position, sourceNode.transform.position, 1f));
            arrowLine.SetEndPoint(Player.EndPoint(sourceNode.transform.position, destinationNode.transform.position, 1.5f));
        }


        // Set the position of the transition label
        Vector3 transitionLabelPos = arrowLine.GetMidpoint();
        transitionLabelPos.z = transitionLabel.transform.position.z;
        transitionLabel.transform.position = transitionLabelPos;

        picker.pickerButton.UpdateAcceptedCharacters(picker.acceptedCharacters);
    }

    public void CommitTransition() {
        arrowLine.Temporary = false;
    }
    
    public void DestroyTransition() {
        sourceNode?.outgoingTransitions.Remove(this);
        destinationNode?.incomingTransitions.Remove(this);
        Destroy(gameObject);
    }

    void OnDestroy() {
        Player.Instance.stateTransitions.Remove(this);
    }

}
