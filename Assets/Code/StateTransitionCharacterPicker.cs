﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class StateTransitionCharacterPicker : MonoBehaviour
{
    ShapesToggleButton toggleButton;

    [SerializeField]
    TextMeshPro characterLabel;

    public char character {get; private set; } = 'a';

    StateTransitionPicker parent;

    private bool _picked = false;
    
    public bool Picked {
        get {
            return _picked;
        }

        set {
            toggleButton.Toggled = value;
        }
    }

    void Awake() {
        toggleButton = GetComponent<ShapesToggleButton>();
    }

    public void SetupWithCharacter(char c, StateTransitionPicker parent) {
        character = c;
        characterLabel.text = character.ToString();

        this.parent = parent;

        toggleButton.toggled += UpdateParentTransition;
    }

    public void UpdateParentTransition(bool toggled) {
        _picked = toggled;
        this.parent.UpdateAcceptedCharacters();
    }
}
