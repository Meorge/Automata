﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Shapes;
public class StateTransitionEnableCharacterPickerButton : ShapesButton
{
    [SerializeField]
    StateTransitionPicker picker;

    public TextMeshPro label;

    private Rectangle rectangle;

    void Awake() {
        rectangle = GetComponent<Rectangle>();
        clicked += TogglePickerVisible;
    }

    public void TogglePickerVisible() {
        picker.ToggleVisibility();
    }

    public void UpdateAcceptedCharacters(string chars) {
        string acceptedCharacters = "";
        foreach (char c in chars) {
            acceptedCharacters += c + ",";
        }
        if (acceptedCharacters.Length >= 2) acceptedCharacters = acceptedCharacters.Substring(0, acceptedCharacters.Length - 1);
        else acceptedCharacters = "ε";

        label.text = acceptedCharacters;

        rectangle.Width = 0.5f + 0.31f * chars.Length;
    }
}
