﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Shapes;
using TMPro;
using DG.Tweening;

public class StateTransitionPicker : MonoBehaviour
{
    [SerializeField]
    Rectangle rectangle;
    RectTransform rectangleRectTransform;

    [SerializeField]
    RectTransform container;
    HorizontalLayoutGroup containerLayout;

    [SerializeField]
    StateTransition stateTransition;

    [Header("Prefabs")]
    [SerializeField]
    StateTransitionCharacterPicker languageCharacterPrefab;

    public float spacing = 0.1f;

    public string acceptedCharacters = "";

    List<StateTransitionCharacterPicker> characterPickers = new List<StateTransitionCharacterPicker>();

    public bool Visible { get; private set; } = false;

    public StateTransitionEnableCharacterPickerButton pickerButton;

    void Start() {
        containerLayout = container.GetComponent<HorizontalLayoutGroup>();
        rectangleRectTransform = rectangle.GetComponent<RectTransform>();
        SetUpButtonsForLanguage();
        SetOut();
    }

    void SetUpButtonsForLanguage() {
        // destroy all current children
        foreach (Transform child in container.transform) {
            Destroy(child.gameObject);
        }

        characterPickers.Clear();

        // Create a new prefab item for each thing in the current language
        foreach (char c in Player.Instance.alphabet) {
            StateTransitionCharacterPicker newInstance = Instantiate(languageCharacterPrefab);
            newInstance.transform.SetParent(container, false);

            newInstance.SetupWithCharacter(c, this);

            characterPickers.Add(newInstance);
        }
    }


    void Update() {
        containerLayout.spacing = spacing;

        float newWidth = (Player.Instance.alphabet.Length * 0.5f) + ((Player.Instance.alphabet.Length + 2) * spacing);
        rectangleRectTransform.sizeDelta = new Vector2(newWidth, rectangleRectTransform.sizeDelta.y);
        rectangle.Width = newWidth;
    }

    public void UpdateAcceptedCharacters() {
        acceptedCharacters = "";
        foreach (StateTransitionCharacterPicker p in characterPickers) {
            if (p.Picked) acceptedCharacters += p.character;
        }
    }

    public void ToggleVisibility() {
        if (Visible) AnimateOut();
        else AnimateIn();
    }

    public void AnimateIn() {
        transform.DOScale(1f, 0.3f).SetEase(Ease.OutBack);
        Visible = true;
    }

    public void AnimateOut() {
        transform.DOScale(0f, 0.3f).SetEase(Ease.InBack);
        Visible = false;
    }

    public void SetOut() {
        transform.localScale = Vector3.zero;
        Visible = false;
    }
}
