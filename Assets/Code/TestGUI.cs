﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Shapes;

public class TestGUI : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI testStringLabel, testStringPassLabel;

    [SerializeField]
    private Rectangle rectangle;

    private Test test;

    public void SetTest(Test test) {
        this.test = test;
        this.test.guiItem = this;

        if (this.testStringLabel != null) this.testStringLabel.text = this.test.testString;
        else Debug.LogError("Test string label isn't assigned");

        if (this.testStringPassLabel != null) this.testStringPassLabel.text = this.test.shouldPass ? "Should accept" : "Should reject";
        else Debug.LogError("Test string lass label isn't assigned");
    }

    public void TestCompleted(bool passed = true) {
        Debug.Log($"Passed: {passed}; Should pass: {test.shouldPass}");
        if ((passed && test.shouldPass) || (!passed && !test.shouldPass)) TestPassed();
        else TestFailed();
    }
    public void TestPassed() {
        if (this.testStringPassLabel != null) this.testStringPassLabel.text = this.test.shouldPass ? "Accepted" : "Rejected";
        else Debug.LogError("Test string lass label isn't assigned");

        rectangle.Color = ColorsManager.Instance.testPassedColor;
    }

    public void TestFailed() {
        if (this.testStringPassLabel != null) this.testStringPassLabel.text = this.test.shouldPass ? "Should have accepted" : "Should have rejected";
        else Debug.LogError("Test string lass label isn't assigned");

        rectangle.Color = ColorsManager.Instance.testFailedColor;
    }
}
